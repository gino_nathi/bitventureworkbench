﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BitventureWorkbench.Globals;

namespace BitventureWorkbench.Utilities
{
    public partial class AddEditHoliday : DevExpress.XtraEditors.XtraForm
    {
        private int _currentEntityID = 0;

        private ActionOperations _actionOperator = ActionOperations.Add;

        private HolidayDto _holidayData = new HolidayDto();


        public AddEditHoliday()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void SetToAdd()
        {
            _actionOperator = ActionOperations.Update;
        }

        public void SetToEdit(HolidayDto holiday)
        {
            if (holiday != null)
            {
                _holidayData = holiday;
                _actionOperator = ActionOperations.Update;
            }

            else
            {
                MessageBox.Show("Holiday data cannot be empty");
            }
        }

        private void AddEditHoliday_Load(object sender, EventArgs e)
        {
            if (_actionOperator == ActionOperations.Update)
            {
                if (_actionOperator == ActionOperations.Add)
                {
                    dateHoliday.DateTime = DateTime.Today; 
                }

                else
                {
                    if (_holidayData.Date > Common.InitialDate)
                        dateHoliday.DateTime = _holidayData.Date;

                    if (!string.IsNullOrEmpty(_holidayData.Description))
                        txtDescription.Text = _holidayData.Description;

                    if (_actionOperator == ActionOperations.Update)
                        this.Text = "Edit note";
                    else
                        this.Text = "Add note";
                }
            }
            else
            {
                MessageBox.Show("Setup not completed.");
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dateHoliday.DateTime > Common.InitialDate)
            {
                if (!string.IsNullOrEmpty(txtDescription.Text))
                {
                    _holidayData.Description = txtDescription.Text;
                    _holidayData.Date = dateHoliday.DateTime;
                    _holidayData.CanOperate = CheckEditCanOperate.Checked;

                    if (_actionOperator == ActionOperations.Add)
                    {
                        // Add new holiday
                    }

                    else
                    {
                        // Update Holiday
                    }
                }
                else
                {
                    MessageBox.Show("Please enter holiday");
                }
            }

            else
            {
                MessageBox.Show("Please select holiday date");
            }
        }
    }
}