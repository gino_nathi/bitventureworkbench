﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitventureWorkbench.Globals
{
    public class Common
    {
        #region Date Utilities

        /// <summary>
        /// Gets the initial date value being used to check for valid datetime values. 
        /// </summary>
        public static DateTime InitialDate
        {
            get { return new DateTime(1900, 1, 1); }
        }
        #endregion
    }
}
