﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitventureWorkbench.Globals
{
    #region Results types

    public struct Result
    {
        public int resultCode;
        public string resultMessage;
    }

    public struct ResultAdd
    {
        public int resultCode;
        public string resultMessage;
        public int NewEntityID;
    }

    public struct ResultAddTX
    {
        public int resultCode;
        public string resultMesssage;
        public long NewEntityID;
    }

    public struct ResultDetail
    {
        public int resultCode;
        public string summaryResultMesssage;
        public List<string> detailResultMesssage;
    }

    public struct ResultExistanceCheck
    {
        public int existenceCheckresultCode;
        public string existenceCheckResultMessage;
        public bool DoesExist;
    }

    #endregion

    #region CodeExecution

    public class CodeExecution
    {
        static public ResultAdd InsertSuccessful(int newEntityId, string message = null)
        {
            return new ResultAdd
            {
                NewEntityID = newEntityId,
                resultCode = 0,
                resultMessage = string.IsNullOrEmpty(message) ? "Added successfully" : message,
            };
        }

        static public ResultAdd InsertFailed(string message = "")
        {
            return new ResultAdd
            {
                NewEntityID = 0,
                resultCode = 1,
                resultMessage = message,
            };
        }

        static public Result Succeeded(string message = "")
        {
            return new Result
            {
                resultCode = 0,
                resultMessage = message,
            };
        }

        static public Result NotFound(string message = "")
        {
            return new Result
            {
                resultCode = 1,
                resultMessage = message,
            };
        }

        static public Result Failed(string message = "")
        {
            return new Result
            {
                resultCode = 2,
                resultMessage = message,
            };
        }

        static public Result Duplicate(string message = "")
        {
            return new Result
            {
                resultCode = 3,
                resultMessage = message,
            };
        }


        static public ResultExistanceCheck Exists(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 0,
                existenceCheckResultMessage = message,
                DoesExist = true,
            };
        }

        static public ResultExistanceCheck DoesNotExists(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 0,
                existenceCheckResultMessage = message,
                DoesExist = false,
            };
        }

        static public ResultExistanceCheck FailedExistance(string message = "")
        {
            return new ResultExistanceCheck
            {
                existenceCheckresultCode = 1,
                existenceCheckResultMessage = message,
                DoesExist = false,
            };
        }

    }

    #endregion

    public enum ActionOperations
    {
        Add = 0,
        Update = 1,
        View = 2,
    }
}
